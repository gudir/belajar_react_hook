import React from 'react'
import { useState } from 'react'
import useLocalStorage from '../hooks/useLocalStorage'

export default function BelajarUseDebug() {
    const [firstName, setFirstname] = useLocalStorage("firstName", "irawan");
    const [lastName, setLastname] = useState("Kasep");
    return (
        <div>
            First{" "}
            <input type="text" value={firstName} onChange={(e) => setFirstname(e.target.value)} />
            Last:{" "}
            <input type="text" value={lastName} onChange={(e) => setLastname(e.target.value)} />
        </div>
    )
}
