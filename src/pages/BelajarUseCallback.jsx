import React, { useState } from 'react'
import { useCallback } from 'react';
import List from '../components/ComponentUseCallback/List';

export default function BelajarUseCallback() {
    const [number, setNumber] = useState(0);
    const [dark, setDark] = useState(false);

    const getItems = useCallback( () => {
        return [number, number + 1, number + 2]
    }, [number])

    const theme = {
        backgroundColor: dark ? '#333' : '#FFF',
        color: dark ? '#FFF' : '#333',
    }
  return (
    <div style={theme}>
        <input type="number" value={number} onChange={(e) => setNumber(parseInt(e.target.value))}/>
        <button onClick={() => setDark(prevDark => !prevDark)}>Toggle Theme</button>
        {/* kalo tanpa useCallback, component List ini akan di update ketika ada perubahan state, 
        contohnya button toggle theme diatas ketika di click , maka component List akan di re render */}
        <List getItems={getItems} /> 
    </div>
  )
}
