import React, { useState } from 'react'
import { useEffect } from 'react';
import { useDeferredValue } from 'react';
import { useMemo } from 'react';

export default function List({ input }) {
    const deferredInput = useDeferredValue(input);
    const LIST_SIZE = 10000;

    const list = useMemo(() => {
        const arr = [];
        for(let i=0; i < LIST_SIZE; i++) {
            arr.push(<div key={i}>{deferredInput}</div>);
        }
        return arr;
    }, [deferredInput]);
       
   //untuk cek perbedaan input dengan deferredInput,
   //untuk input langsung ada m kalo deferredInput akan ada value ketika stop mengetik input
   useEffect(() => {
        console.log(`input: ${input} \nDeferredValue: ${deferredInput}`);
   }, [input, deferredInput])
    return list;
}
