import React from 'react'
import EmailForm from '../components/ComponentUseId/EmailForm'

export default function BelajaruseId() {
  return (
    <div>
        <EmailForm />
        <article style={{ marginBlock: "1rem" }}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur corporis tempora cum repudiandae neque! Id beatae tempora sequi ex reiciendis, vero ea tempore neque eum quam inventore repellendus quidem nostrum! Quisquam harum natus amet officia provident corrupti laborum atque. Ducimus nihil quas quae. Incidunt saepe corrupti beatae ducimus. Ut, quod. Voluptatum quam facilis explicabo laboriosam consequuntur error omnis quisquam. Excepturi ut distinctio minima tenetur! Ut possimus dignissimos perferendis voluptates expedita commodi et, incidunt assumenda mollitia, quos accusantium? Maiores porro perferendis fugiat laboriosam, similique cupiditate ex dolore aliquid culpa officia quia deleniti corporis. Perspiciatis veritatis dolorem eos fugit illum laborum aliquam.
        </article>
        {/* Ketika label nya di klik , inputannya akan focus ke EmailForm yang di atas 
        , karena memiliki ID yang sama, oleh karena itu cara mengatasinya dengan hook useId */}
        <EmailForm />
    </div>
  )
}
