import React, { useState, useRef } from 'react'

export default function BelajarUseReff() {
  const [name, setName] = useState('');
  //catatan: useRef mirip dengan document.getElementById di vanilla javascript
  //jadi useRef bisa mendapatkan apapun dari element tersebut
  const inputFocus = useRef();

  const focus = () => {
    console.log(inputFocus.current);
    //contoh inputFocus di bind dengan element html ref={inputFocus}
    //di sini ketika button di click , inputan dengan ref inputFocus akan di focus inputkan
    inputFocus.current.focus();
  }

  return (
    <>
      {/* ref={inputFocus} kalo di javascript itu document.getElementById('inputFocus') */}
      <input ref={inputFocus} type="text" onChange={ (e) => setName(e.target.value) } />
      <div>My Name is {name}</div>
      <button onClick={focus}>focus to input</button>
    </>
  )
}
