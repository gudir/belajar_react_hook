import React,{ useTransition,useState } from 'react'

export default function BelajarUseTransition() {
    const [isPending, startTransition] = useTransition()
    const [input, setInput] = useState("");
    const [lists, setLists] = useState([]);

    const LIST_SIZE = 10000;

    function handleChange(e){
        setInput(e.target.value)
        startTransition(() => {
            const arr = [];
            for(let i=0; i < LIST_SIZE; i++) {
                arr.push(e.target.value);
            }
            setLists(arr);
        })
    }
   
    return (
        <div>
            <input type="text" value={input} onChange={handleChange} />
            { isPending ? 'Loading ...' 
                :lists.map((item, index) => {
                console.log(item);
                return <div key={index}>{item}</div>
            })}
        </div>
    )
}
