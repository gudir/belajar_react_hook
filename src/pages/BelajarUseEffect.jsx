import React, { useEffect, useState } from 'react'

export default function BelajarUseEffect() {
    const [resourceType, setResourceType] = useState('posts')
    const [items, setItems] = useState([])
    const [sizeWindow, setSizeWindow] = useState(window.innerWidth)

    //UseEffect tanpa depedency, akan terus di panggil ketika ada perubahan state (componentDidMount)
    //contohnya saat kita click button Post, users, comment setiap di klik, 
    //atau semua yang ada perubahan state, useeffect ini akan di panggil terus menerus
    useEffect(() => {
        console.log("ini akan di render terus");
    });

    //useEffect dengan dependency tetapi kosong (onMounted)
    //useEffect ini akan di panggil satu kali saat pertama kalo browser me render halaman/komponen
    //mirip seperti window onload lah
    useEffect(() => {
        console.log("Satu kali di panggil");
    },[]);

    //(componentDidUpdate)
    //useEffect dengan depedency , useEffect akan me re-render saat ada perubahan state yang ada di dalam depedency
    //contoh disini , komponen aka me re-render saat ada perubahan resourceType
    useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/${resourceType}`)
            .then(response => response.json())
            .then(json => setItems(json))
    }, [resourceType]);

    //(componentWillUnmount)
    //sama dengan useEffect dengan depedency , tetapi dengan tambahan menghapus/clean up component dari DOM
    //agar aplikasi tidak Leak memory
    //contoh penggunaan
    useEffect(() => {
        //function untuk mendapatkan lebar saat di resize
        //function ini akan terus di panggil saat kita me resize browser
        //mengakibatkan leak memory karena function ini menumpuk di dalam DOM browser
        window.addEventListener("resize", () => { setSizeWindow(window.innerWidth) });

        //oleh karena itu kita harus hapus/cleanup setiap function Resize di panggil 
        return () => {
            //console.log("Hapus dari DOM");
            window.removeEventListener("resize", () => { console.log("Hapus dari DOM"); });
        }
    }, [sizeWindow]);
  return (
    <>
    {sizeWindow}
    <div>
        <button onClick={() => setResourceType('posts')}>Posts</button>
        <button onClick={() => setResourceType('users')}>Users</button>
        <button onClick={() => setResourceType('comments')}>comments</button>
    </div>
    <h1>{resourceType}</h1>
    {items.map((item) => {
        return <pre key={item.id}>{JSON.stringify(item)}</pre>
    })}
    </>
  )
}
