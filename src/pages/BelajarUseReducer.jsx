import React, { useReducer, useState } from 'react'

function reducer(state, action) {
    switch(action.type) {
        case 'increment':
            return { count:  state.count + 1}
        case 'decrement':
            return { count:  state.count - 1}
        default :
            return state
    } 
}

export default function BelajarUseReducer() {
    //without reducer
//   const [count,setCount] = useState(0);

//   const increment = () => {
//     setCount(prevCount => prevCount + 1);
//   }

//   const decrement = () => {
//     setCount(prevCount => prevCount - 1);
//   }

    const [state, dispatch] = useReducer(reducer, { count: 0 })


    const increment = () => {
        dispatch({ type: 'increment' });
    }

    const decrement = () => {
        dispatch({ type: 'decrement' })
    }

    return (
    <div>
        <button onClick={decrement}>-</button>
        <span>{state.count}</span>
        <button onClick={increment}>+</button>
    </div>
  )
}
