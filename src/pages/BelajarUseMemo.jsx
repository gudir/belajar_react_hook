import React, { useMemo, useState } from 'react'

export default function BelajarUseMemo() {
    const [number, setNumber] = useState(0)
    const [dark, setDark] = useState(false)

    //function slowFunction akan terus di jalankan ketika ada perubahan state
    //const doubleNumber = slowFunction(number)

    //contoh 1
    //function slowFunction akan di jalankan ketika ada perubahan di state number
    const doubleNumber = useMemo(() => {
        return slowFunction(number);
    }, [number]);

    //contoh2
    const themeStyle = useMemo(() => {
        return {
            backgroundColor: dark ? 'black' : 'white',
            color: dark ? 'white' : 'black'
        }
    }, [dark]);

  return (
    <div>
        <input type="number" value={number} onChange={ (e) => setNumber(parseInt(e.target.value)) } />
        <button onClick={ () => setDark( prevDark => !prevDark ) }>CHange theme</button>
        <div style={themeStyle}>{doubleNumber}</div>
    </div>
  )
}

function slowFunction(num) {
    console.log('Calling slow function')
    for(let i=0; i <= 1000000000; i++) {

    }

    return num * 2;
}