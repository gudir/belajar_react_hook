import { Link, Route, Routes } from 'react-router-dom'
import BelajarUseCallback from './pages/BelajarUseCallback'
import BelajarUseContext from './pages/BelajarUseContext'
import BelajarUseDebug from './pages/BelajarUseDebug'
import BelajarUseDeferredValue from './pages/BelajarUseDeferredValue'
import BelajarUseEffect from './pages/BelajarUseEffect'
import BelajaruseId from './pages/BelajaruseId'
import BelajarUseImperativeHandle from './pages/BelajarUseImperativeHandle'
import BelajarUseLayoutEffect from './pages/BelajarUseLayoutEffect'
import BelajarUseMemo from './pages/BelajarUseMemo'
import BelajarUseReducer from './pages/BelajarUseReducer'
import BelajarUseReff from './pages/BelajarUseReff'
import BelajarUseTransition from './pages/BelajarUseTransition'
import BelajarUseLocalStorage from './pages/custom_hooks/BelajarUseLocalStorage'
import Home from './pages/Home'

function App() {

  return (
    <div className="App">
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/belajar-useeffect">Belajar UseEffect</Link>
        </li>
        <li>
          <Link to="/belajar-usememo">Belajar UseMemo</Link>
        </li>
        <li>
          <Link to="/belajar-reff">Belajar UseReff</Link>
        </li>
        <li>
          <Link to="/belajar-usecontext">Belajar UseContext</Link>
        </li>
        <li>
          <Link to="/belajar-usereducer">Belajar UseReducer</Link>
        </li>
        <li>
          <Link to="/belajar-usecallback">Belajar UseCallback</Link>
        </li>
        <li>
          Custom Hook
          <ul>
            <li><Link to="/custom-hook/belajar-uselocalstorage" >Hook useLocalStorage</Link></li>
          </ul>
        </li>
        <li>
          <Link to="/belajar-uselayouteffect">Belajar UseLayoutEffect</Link>
        </li>
        <li>
          <Link to="/belajar-usetransition">Belajar UseTransition</Link>
        </li>
        <li>
          <Link to="/belajar-usedeferredvalue">Belajar UseDeferredValue</Link>
        </li>
        <li>
          <Link to="/belajar-useimperativehandle">Belajar UseImperativeHandle</Link>
        </li>
        <li>
          <Link to="/belajar-usedebug">Belajar UseDebug</Link>
        </li>
        <li>
          <Link to="/belajar-useid">Belajar UseId</Link>
        </li>
      </ul>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/belajar-useeffect' element={<BelajarUseEffect />} />
        <Route path='/belajar-usememo' element={<BelajarUseMemo />} />
        <Route path='/belajar-reff' element={<BelajarUseReff />} />
        <Route path='/belajar-usecontext' element={<BelajarUseContext />} />
        <Route path='/belajar-usereducer' element={<BelajarUseReducer />} />
        <Route path='/belajar-usecallback' element={<BelajarUseCallback />} />
        <Route path='/custom-hook/belajar-uselocalstorage' element={<BelajarUseLocalStorage />} />
        <Route path='/belajar-uselayouteffect' element={<BelajarUseLayoutEffect />} />
        <Route path='/belajar-usetransition' element={<BelajarUseTransition />} />
        <Route path='/belajar-usedeferredvalue' element={<BelajarUseDeferredValue />} />
        <Route path='/belajar-useimperativehandle' element={<BelajarUseImperativeHandle />} />
        <Route path='/belajar-usedebug' element={<BelajarUseDebug />} />
        <Route path='/belajar-useid' element={<BelajaruseId />} />
      </Routes>
    </div>
  )
}

export default App
