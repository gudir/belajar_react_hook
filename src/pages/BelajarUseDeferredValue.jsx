import React, { useState } from 'react'
import List from '../components/ComponentUseDeferredValue/List';

export default function BelajarUseDeferredValue() {
    const [input, setInput] = useState("");

    function handleChange(e){
        setInput(e.target.value)
    }
    
    return (
        <div>
            <input type="text" value={input} onChange={handleChange} />
            <List input={input} />
        </div>
    )
}
