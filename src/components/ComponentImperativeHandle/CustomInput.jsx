import React from 'react'
import { useImperativeHandle } from 'react';

function CustomInput({ style, ...props }, ref) {
  useImperativeHandle(
    ref,
    () => {
      return {
        alertHi: () => alert("Hello bro")
      }
    }
  ,[]);

  return (
    <div>
      <input 
        ref={ref}
        type="text" 
        {...props}
        style={{
          border: 'none',
          backgroundColor: 'lightpink',
          padding: '.25em',
          borderBottom: '.1em solid black',
          borderTopLeftRadius: '.25em',
          borderTopRightRadius: '.25em',
          ...style,
        }}  
      />
    </div>
  )
}

export default React.forwardRef(CustomInput);