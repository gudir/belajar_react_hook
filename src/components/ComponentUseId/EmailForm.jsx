import React from 'react'
import { useId } from 'react'

export default function EmailForm() {
    // hooks ini generate unique id
    const id = useId();
  return (
    <div>
        <label htmlFor={id}>Email</label>
        <input type="email" id={id} />
    </div>
  )
}
