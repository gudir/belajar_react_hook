import React, { useEffect, useLayoutEffect, useState } from 'react'

export default function BelajarUseLayoutEffect() {
    // Catatan : useLayoutEffect sama dengan function useEffect , bedanya useLayoutEffect itu synchronous
    //kalo useEffect itu asynchronous
    const [value, setValue] = useState(0);
    const [value2, setValue2] = useState(0);
    
    // contoh jika pakai useEffect akan ada flicker di tampilannya
    useEffect(() => {
        if (value === 0) {
            setValue(10 + Math.random() * 200);
        }
    }, [value]);

    console.log('render', value);

    useLayoutEffect(() => {
        if (value2 === 0) {
            setValue2(10 + Math.random() * 200);
        }
    }, [value2]);
    console.log('render', value2);

    return (
        <>
            <div className="App">
                <p>Value using useEffect: {value}</p>
                <button onClick={() => setValue(0)}>
                    Generate Random Value
                </button>
                <span>ada flickering saat ganti value</span>
            </div>

            <div className="App">
                <p>Value using useLayoutEffect: {value2}</p>
                <button onClick={() => setValue2(0)}>
                    Generate Random Value
                </button>
                <span>tidak ada flickering saat ganti value</span>
            </div>
        </>
    );
}
