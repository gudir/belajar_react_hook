import React, { useState } from 'react'
import { useEffect } from 'react';

export default function List({getItems}) {
    const [items, setItems] = useState([]);

    useEffect(() => {
        setItems(getItems())
        console.log('Updating Item');
    }, [getItems]);
  return (
    <div>{items.map((item, index) => (
        <p key={index}>{item}</p>
    ))}</div>
  )
}
