import React from 'react'
import { useState } from 'react'
import useLocalStorage from '../../hooks/useLocalStorage';

export default function BelajarUseLocalStorage() {
    const [name, setName] = useLocalStorage('');
    
    return (
        <div>
            <input type="text" value={name} onChange={e => setName(e.target.value)} />
            <div>{name}</div>
        </div>
  )
}
