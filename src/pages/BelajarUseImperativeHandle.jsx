import React from 'react'
import { useState } from 'react'
import { useRef } from 'react'
import CustomInput from '../components/ComponentImperativeHandle/CustomInput'

export default function BelajarUseImperativeHandle() {
    const inputRef = useRef()
    const [value, setValue] = useState("red")
  return (
    <div>
        <CustomInput 
            ref={inputRef}
            value={value}
            onChange={(e) => setValue(e.target.value)}
        />
        <button onClick={() => inputRef.current.alertHi()}>Focus</button>
    </div>
  )
}
