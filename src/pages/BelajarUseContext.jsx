import React from 'react'
import FunctionContextComponent from '../components/ComponentUseContext/FunctionContextComponent';
import {ThemeProvider} from '../provider/ThemeContextProvider';

export default function BelajarUseContext() {

  return (
    <>
        <ThemeProvider>
            <FunctionContextComponent />
        </ThemeProvider>
    </>
  )
}
